const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const message = `
Choose an option:
1. Read package.jason
2. Display OS info
3. Start HTTP server
-------------------
Type a number:`;

rl.question(message, answer => {
  if (answer == 1) {
    // Read package.json
    const pjson = require("./package.json");
    console.log(pjson);
  } else if (answer == 2) {
    // Display OS info
    require("./readline");
  } else if (answer == 3) {
    // Start Http server
    require("./http.js");
  } else {
    console.log("Invalid option.");
  }
  rl.close();
});
