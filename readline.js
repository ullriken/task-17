const os = require("os");
    console.log("Getting OS info...");
    console.log(
      "SYSTEM MEMOORY:  " +
        (os.totalmem() / 1024 / 1024 / 1024).toFixed(2) +
        " GB"
    );
    console.log(
      "FREE MEMOORY:  " + (os.freemem() / 1024 / 1024 / 1024).toFixed(2) + " GB"
    );
    console.log("CPU CORES:  " + os.cpus().length);
    console.log("ARCH:  " + os.arch);
    console.log("PLATFORM:  " + os.platform);
    console.log("RELEASE:  " + os.release);
    console.log("USER:  " + os.userInfo().username);